#!/bin/sh
#
# Copyright (c) 2019 Wesley Eddy (wesley.m.eddy@gmail.com)

EXAMPLE_LISP=$(cat <<EOF
(asdf:load-system :cl-hackrf)

(defpackage #:cl-hackrf-test
  (:use #:cl #:cffi #:hackrf))

(in-package #:cl-hackrf-test)

(defun hackrf-read-test-example ()
   ;;; necessary call to initialize libhackrf state
  (hackrf-init)
  ;;; create a callback function to handle each buffer read from the device
  (define-hackrf-callback callback-fn (hackrf-transfer-ptr)
    (with-foreign-slots
      ((transfer-device treansfer-buffer transfer-buffer-len
        transfer-valid-len transfer-rx-ctx transfer-tx-ctx)
       hackrf-transfer-ptr (:struct hackrf-transfer))
     (format T "read ~a bytes (buffer of ~A)~%"
             transfer-valid-len transfer-buffer-len))
    0)

  (with-hackrf (device 0)
    (set-center-frequency device (MHz 88))
    (set-sample-rate device (coerce (MHz 1) 'double-float))

    (hackrf-start-rx device (callback callback-fn) (null-pointer))
    ;;; capture for 5 seconds
    (sleep 5)
    (hackrf-stop-rx device))

  ;;; cleanup libhackrf state
  (hackrf-exit))
(hackrf-read-test-example)
EOF
)

echo "${EXAMPLE_LISP}" | sbcl

