;;; Copyright (c) 2019 Wesley M. Eddy (wesley.m.eddy@gmail.com)

(defsystem cl-hackrf
  :author "Wesley Eddy <wesley.m.eddy@gmail.com>"
  :license "MIT"
  :version "0.1"
  :homepage "https://bitbucket.org/weddy/cl-hackrf"
  :description "CFFI-based interface from Common Lisp to libhackrf."
  :depends-on (:cffi)
  :components ((:module "src"
                :components ((:file "hackrf")))))

