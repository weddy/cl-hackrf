;;; Common Lisp routines for working w/ HackRF hardware
;;;
;;; Copyright (c) 2019, Wesley Eddy (wesley.m.eddy@gmail.com).
;;; All rights reserved.  Refer to the LICENSE.txt file within the parent
;;; directory
;;;
;;; Includes ideas inherrited from http://github.com/malcolmstill/cl-rtlsdr

(defpackage :hackrf
  (:use :common-lisp :cffi)
  (:export ;;; convenience functions for frequencies
           :MHz
           ;;; C function calls
           :hackrf-init
           :hackrf-exit
           :hackrf-device-list
           :hackrf-device-list-open
           :hackrf-device-list-free
           :hackrf-close
           :set-center-frequency
           :set-sample-rate
           :hackrf-start-rx
           :hackrf-stop-rx
           ;;; data structures
           :hackrf-transfer
           :transfer-device
           :transfer-buffer
           :transfer-buffer-len
           :transfer-valid-len
           :transfer-rx-ctx
           :transfer-tx-ctx
           ;;; convenience macro to wrap library + device setup/teardown
           :with-hackrf
           ;;; convenience macro to create callback functions
           :define-hackrf-callback
           ;;; not really needed
           :hackrf-read-test-example))

(in-package :hackrf)

(define-foreign-library libhackrf
  (T (:default "libhackrf")))

(use-foreign-library libhackrf)

(defun MHz (m) (* 1000 1000 m))

(defcstruct hackrf-transfer
  (transfer-device :pointer)
  (transfer-buffer :pointer) ;;; uint8_t *
  (transfer-buffer-len :int32)
  (transfer-valid-len :int32)
  (transfer-rx-ctx :pointer)
  (transfer-tx-ctx :pointer))

(defcfun ("hackrf_init" hackrf-init) :int32)

(defcfun ("hackrf_exit" hackrf-exit) :int32)

(defcfun ("hackrf_device_list" hackrf-device-list) :pointer)

(defcfun ("hackrf_device_list_open" hackrf-device-list-open) :int32
  (device-list-ptr :pointer)
  (index :int32)
  (device-ptr :pointer))

(defcfun ("hackrf_device_list_free" hackrf-device-list-free) :void
  (device-list-ptr :pointer))

(defcfun ("hackrf_close" hackrf-close) :int32
  (device-ptr :pointer))

;;; set device center frequency in Hz
(defcfun ("hackrf_set_freq" set-center-frequency) :int32
  (device :pointer)
  (freq_hz :uint64))

;;; set the device sample rate in Hz
(defcfun ("hackrf_set_sample_rate" set-sample-rate) :int32
  (device :pointer)
  (freq_hz :double))

(defcfun ("hackrf_start_rx" hackrf-start-rx) :int32
  (device :pointer)
  (callback :pointer)
  (rx-ctx :pointer))

(defcfun ("hackrf_stop_rx" hackrf-stop-rx) :int32
  (device :pointer))

(defmacro with-hackrf ((device index) &body body)
  `(with-foreign-object (device-ptr :pointer)
     (let* ((device-list-ptr (hackrf-device-list))
            (retval (hackrf-device-list-open device-list-ptr
                                             ,index device-ptr)))
       (if (not (= 0 retval))
           (error "Failed to open device at index ~A (error ~A).~%"
                  ,index retval)
           (let* ((,device (mem-ref device-ptr :pointer)))
             (unwind-protect (progn ,@body)
               (hackrf-close ,device)
               (hackrf-device-list-free device-list-ptr)))))))

;;; callbacks need to treat transfer as a pointer to :hackrf-transfer type
(defmacro define-hackrf-callback (name (transfer) &body body)
  `(progn
     (defcallback ,name :int32
       ((,transfer (:pointer (:struct hackrf-transfer))))
       ,@body)))

