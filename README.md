# cl-hackrf

HackRF library for Common Lisp

**Copyright (c) 2019, Wesley Eddy (wesley.m.eddy@gmail.com)**

This software assists Common Lisp programmers working with HackRF Software
Defined Radio (SDR) devices on Linux or other supported platforms.  It provides
Common Lisp bindings to the C libhackrf as well as some convenient macros and
other definitions useful in writing SDR and Digital Signal Processing (DSP)
applications that use HackRF hardware for radio signal sampling, Analog to
Digital and Digital to Analog conversion (ADC/DAC).

Refer to the included LICENSE.txt file for license information.

This software has been tested using HackRF One devices on Ubuntu 18.04, with
the system-provided packages for:

* sbcl
* cl-quicklisp
* cl-asdf
* libhackrf-dev

CFFI is used for interfacing with libhackrf.  CFFI and its dependencies are
loadded via ASDF (rather than via the cl-cffi Ubuntu package) in our tests.
